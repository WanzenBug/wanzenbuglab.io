+++
title = "About me"

draft = false
+++

Hey there, looks like you found my blog and want to know more about me.

I am Moritz Wanzenböck, a developer and part-time student of mathematics. Among my favorite topics as a 
developer are writing high performance algorithms/data structures, operating systems and automation. As a 
mathematician I am also leaning close to IT, my personal favorites are cryptography and coding theory (as in 
compression algorithms and error correction).

I am currently located in Vienna, Austria. After having finished my bachelor's degree in mathematics I am on the 
lookout for a fitting master's programme. 

<div class="contact">
<h2>Contact</h2>
<p>
If you have any questions, you can reach me via <a href="mailto:moritz@wanzenbug.xyz">mail</a> or <a href="https://www.linkedin.com/in/moritz-wanzenböck">linked.in</a>
</p>
<p>
If you have questions about on of my <a href="https://github.com/WanzenBug">Github projects</a>, its best to open an issue right there.
</p>
</div>

## Currently I'm working...

### ... for [LINBIT](https://www.linbit.com/) as a software developer
<div class="working-timerange">(2020-05-01 &mdash; ongoing)</div>

Currently we are working on bringing the power of 20+ years of development on [DRBD](https://www.linbit.com/drbd/) and [LVM](https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux))
to the world of software defined storage in Kubernetes. Project include:
* Creating an open source [operator](https://github.com/piraeusdatastore/piraeus-operator) to deploy our SDS solution
* Developing a k8s scheduler plugin to improve storage locality
* Improving our test set up by running automated tests against freshly provisioned k8s clusters on every merge
* Our in-house CI tools, notably [virter](https://github.com/linbit/virter), a faster alternative to vagrant.

### ... a [pylint plugin] that warns if you import something not specified in setup.py
<div class="working-timerange">(2019-11-02 &mdash; 2019-11-12)</div>

At work we use a single repository to house a bunch of related python packages. Each package should
be installable separately (some of them are "released" to the public, some are private) via pip.
To make sure we don't accidentally miss specifying all (direct) imports, this plugin crawls all
imports and verifies they are referenced in `setup.py`

[pylint plugin]: https://github.com/WanzenBug/pylint-import-requirements/

### ... on a [drop-in replacement] for `dvc run`
`dvc` is a tool for creating reproducible pipelines, originally for data science, but also useful in other areas, such
as machine learning.

One of the main commands when using ìt is `dvc run`, for example:
```console
$ dvc run -f featurize.dvc \
          -d src/featurization.py -d data/prepared \
          -o data/features \
          python src/featurization.py \
                 data/prepared data/features
```
This describes a pipeline stage, which depends on `src/featurization.py` and `data/prepared`
to produce `data/features`.

This is quite nice, but I think it can be improved. Notice that all inputs and
outputs are specified twice, once for `dvc` to record, once for the actual command to run. Say for example
your python code imports another python file. Then you need to also add this to the command line, but it is very easy
to overlook.

My plan is to directly replace `dvc run` with an enhanced version that uses the [ptrace] API available
on linux to record all input files and output files. It would then write a `dvc` compatible file to disk. This has the
benefit that other commands should just work.™

[drop-in replacement]: https://github.com/WanzenBug/dry
[ptrace]: http://man7.org/linux/man-pages/man2/ptrace.2.html 

## Previously I worked...

### ... for [cloudlight.io](https://www.cloudflight.io/) as a software developer
<div class="working-timerange">(2015-07-01 &mdash; 2020-04-30)</div>

We were working on a [cloud platform](https://www.grasp-open.com/) for the Earth Observation community,
providing easy access to the Open Source GRASP framework. Some of the tasks include
* Writing python packages that can be used by scientist to access and analyze satellite data
* Servicing the cloud platform (mostly openshift)
* Writing high performance image processing code (mostly C++)
* Writing a low resource overhead task scheduler (Rust)

## Older projects
* [xch](https://github.com/WanzenBug/xch) a library/CLI tool to swap paths atomically, written in Rust.
* [rqrr](https://github.com/WanzenBug/rqrr) a high performance QR code reader, written in Rust.
  * As a little diversion, I also implemented a rust macro to automatically structs that implement high performance
    finite field arithmetic over any binary field: [g2p](https://github.com/WanzenBug/g2p/)
* [this blog](/) mostly I just wanted to build a fast and responsive website with just CSS and html.
  Mostly to show myself that I could. If you notice any issue with this site, please reach out via the mail linked above.

## Other interests
On most weekends you can find me somewhere in the Austrian Alps, either hiking or climbing up some mountain.
There is a work-in-progress section of this blog dedicated to that [here](/categories/outdoors/)

I also enjoy playing board games, play the occasional video game and cooking.
