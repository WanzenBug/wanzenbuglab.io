+++
title = "Update: meson and CLion"
date = 2020-02-21
draft = false

[taxonomies]
tags = ["meson", "cpp", "tutorial"]
categories = ["programming"]
+++

In a [previous blogpost] I walked you through a quick setup of using `meson` in combination with CLion.
Shortly afterwards I stumbled across a [cool project] which promised to bring all the nice integrations
CLion offers over to any meson project.

<!-- more -->

Sadly the project itself seemed to be abandoned, and no updates happened for some time. `meson` in the
meanwhile did receive some updates, and so the inevitable breakage happened. For example
```
Traceback (most recent call last):
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/bin/mcw", line 10, in <module>
    sys.exit(main())
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/mcw/main.py", line 6, in main
    CMakeWrapper().run(sys.argv)
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/mcw/cmake.py", line 66, in run
    raise e
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/mcw/cmake.py", line 59, in run
    getattr(self, self.command + '_cmd')()
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/mcw/cmake.py", line 182, in generate_cmd
    self.gen_codeblocks_project()
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/mcw/cmake.py", line 466, in gen_codeblocks_project
    output = os.path.join(self.meson.build_dir, self.meson.get_target_filename(target))
  File "/home/sunqi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/mcw/meson.py", line 232, in get_target_filename
    return target['filename'][0]
IndexError: string index out of range
```
Luckily a [fork] which fixed this issue already existed. I then forked to fork to fix some more issues
that cropped up in the meanwhile.

### Install
To install you need a recent version of python (which you should anyways if you are running meson).
```console
$ pip3 install --user git+https://gitlab.com/WanzenBug/meson-cmake-wrapper.git
```
This will install a command `mcw`, which can be used as a drop-in replcement for `cmake`.
If a `meson.build` file is present, `mcw` will do its magic, if not it will fall back to using
the system `cmake` command.

To configure CLion to use it, you need to update your toolchain settings:
1. Head to your Settings -> Build, Execution, Deployment -> Toolchains
2. Set the cmake version from "Bundled" to custom and add the path to `mcw`
And that's it.

[previous blogpost]: @/clion-meson-compiledb.md
[cool project]: https://gitlab.com/prozum/devel/meson-cmake-wrapper/
[fork]: https://gitlab.com/solarliner/meson-cmake-wrapper
