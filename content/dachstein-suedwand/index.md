+++
title = "Dachstein Südwand"

date = 2019-10-20
draft = false

[taxonomies]
categories = ["outdoors"]
+++
Hiking up the imposing flanks of the Dachstein mountain to reach to reach the heights of permanent snow and ice.
<!-- more -->

Some friends and I were in [Ramsau am Dachstein](https://www.openstreetmap.org/relation/50588) last september for some
hiking and other outdoors-y activities. Since the weather forecast only let us have one day of good weather, we decided
to make it count: We wanted to go up the highest mountain around. 

[The Dachstein](https://en.wikipedia.org/wiki/Hoher_Dachstein) is a pretty well-known mountain in austria, almost 3000m
high with big glaciers to the north. Now since some of us were pretty inexperienced, we quickly decided we won't go all
the way to the top. The only way to the summit is using a [via ferrata](https://en.wikipedia.org/wiki/Via_ferrata), so
we agreed that just getting to the glaciers will be challenge enough.

After some more planning we agreed on a route that looked not to difficult for all of us:
Starting from the bottom station of the cable car we planned to hike up the Edelgrieskar, passing by the 
Edelgries glacier and using the Rosmarie-Stollen through the last rock faces to get to the glacier on the other side.

The next day, we started early and drove to the parking lot at the cable car station. From there the first kilometre of
the way passes through some light forest and green meadows before reaching the first rock cliffs at the entrance to the
Edelgrieskar.

{{ article_img(path="dachstein-suedwand/meadow.jpg", alt="Meadows at the start") }}

{{ article_img(path="dachstein-suedwand/kar-entrance.jpg", alt="Entrance to the Edelgrieskar") }}

Walking up the first steeper parts of the hike, we disturbed some chamois, who promptly fled up into higher
regions. Our way continued further into the valley, the steep rock faces of the Koppenkarstein mountain always
reminding us that more challenges would await us. The first of the was soon to come

{{ article_img(path="dachstein-suedwand/kar.jpg", alt="View into the Edelgrieskar valley") }}
{{ article_img(path="dachstein-suedwand/iron-clamps.jpg", alt="First challenging climbing sections") }}

Soon the way grew steeper again, and at one point, it required the aid of some iron clamps drilled into
the rock to continue. Soon after, we were rewarded with our first "glacier" of the day. When I say glacier
its really more of left-over snow from last year. Luckily for us it shrunk to such a degree in the last 
decades that we could pass it with extra equipment and fear of crevices.

{{ article_img(path="dachstein-suedwand/edelgries-glacier.jpg", alt="Our group on the Edelgries glacier") }}

Having successfully navigated the glacier we faced another steep passage, this time without the aid of
iron clamps. Instead we had the choice of up to 3 different ropes. Again we were rewarded for our troubles
with a spectacular view. This time to the cable car station atop the mountain range. The 1000m high cliffs
of the Dachstein were impressive to see from this angle

{{ article_img(path="dachstein-suedwand/rope.jpg", alt="Looking back at our climb up passing the glacier") }}
{{ article_img(path="dachstein-suedwand/suedwand.jpg", alt="Looking at the south face of mount Dachstein") }}

Now we were almost at the top! Passing in front of some of steep rock faces, we finally reached the 
Rosmarie-Stollen, a small tunnel through the mountains, only about 20 metres long. In winter it is used
by skiers to ride all the way back to Ramsau, using the same way we walked up, just reversed. The exit of
this tunnel revealed our next challenge however

{{ article_img(path="dachstein-suedwand/tunnel.jpg", alt="Through the tunnel") }}
{{ article_img(path="dachstein-suedwand/glacier-no-2.jpg", alt="Looking down at our destination") }}
{{ article_img(path="dachstein-suedwand/ladder.jpg", alt="Ladder from below") }}

The exit of the tunnel was about 20 metres above the glacier! And the only way down was one really long
ladder! This took as all by surprise, but in the end we all agreed to climb down. Slowly and taking care 
of each step we all safely reached the bottom. According to some people online the ladder was originally
only 5 metres long, but climate change made the increase in length necessary.

Now the challenges where over, and we joined the masses of tourists brought to the glacier by cable car
on our way to the [Seethalerhütte](http://www.alpenverein.at/seethalerhuette/) directly below the summit of
Dachstein.

{{ article_img(path="dachstein-suedwand/dachstein.jpg", alt="Directly below the Dachstein summit") }}

On our way we chose to use the easy route: we used the cable car.

### Additional resources
[GPX Track](dachstein-suedwand.gpx)
