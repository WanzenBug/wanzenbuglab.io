+++
title = "Koppenkarstein"

date = 2020-08-15
draft = false

[taxonomies]
categories = ["outdoors"]
+++
Returning once more to the Dachstein mountain to climb one of its imposing summits. 
<!-- more -->

On my visit to [Ramsau am Dachstein last year](../dachstein-suedwand/) we managed to scale the imposing south wall. This
year I returned to finally scale one of the many peaks above the glacier. Many of the peaks are only accessible for
rock climbers, but a select few are also reachable via a Via Ferrata.

The peak of choice was the Koppenkarstein, 2863m high (making it the highest point I ever scaled). It already featured
in this blog, its the steep rock faces seen at the end of the Edelgrieskar in last year's entry. The [Irg-2 Via Ferrata]
scales the last section up the south wall right to the top.

[Irg-2 Via Ferrata]: https://www.schladming-dachstein.at/en/regional-and-offerings/tours/Via-ferrata-Irg_td_370648 

So, back to the start: we missed the reservation for the gondola up the mountain, so instead we decided to get up really
early and hike all the way up from the parking lot (1200m altitude difference!). The weather report spoke of another
hot summer's day, so we wanted to escape the heat of the day as best we could.

{{ article_img(path="koppenkarstein/morning.jpg", alt="Early morning view") }}

Starting early, we could stay in the shadow of the surrounding mountains right until we reached the southern glacier,
our goal for today always in view.

{{ article_img(path="koppenkarstein/edelgrieskar.jpg", alt="Edelgrieskar, looking at our goal for today") }}

Soon (read: 2 hours from the parking lot) we reached the glacier, and I got another look of the route we took last year

{{ article_img(path="koppenkarstein/edelgriesgletscher.jpg", alt="Edelgries glacier, looking at last year's route") }}

This time, keeping to the right of the glacier, we reached the start of the Via Ferrata. Putting on our climbing gear,
we soon started scaling the last section to the summit. At this point we were already 900m above our starting altitude,
but there were still 300m more to go. 

{{ article_img(path="koppenkarstein/irg-entrance.jpg", alt="Start of the Irg-2 Via Ferrata") }}

We knew from the description of the Via Ferrata that scaling these last 300m would take almost as long as the 900m from
the start. While the climb itself was not too difficult in a physical sense, the psychological effect of standing 200m
above ground add to an already exhausting climb.

{{ article_img(path="koppenkarstein/irg-back.jpg", alt="Looking back where we came from") }}

Finally, right at noon, we reached the summit of Koppenkarstein, after 4¾ hours, 1200m up. This peak really felt like
we've "conquered" it.

{{ article_img(path="koppenkarstein/summit.jpg", alt="Koppenkarstein summit") }}

From the top, there is an amazing view of the glacier, Ramsau and the Enns Valley far below, and the snow covered peaks
of the Hohen Tauern mountains far in the distance.

{{ article_img(path="koppenkarstein/dachstein.jpg", alt="Looking down at the Dachstein glacier") }}

After a lunch break at the top, we started our decent via the West Ridge Via Ferrata. While it has an easier rating than
our ascent, climbing down is not always a pleasant experience. So while we expected to reach the gondola station in
about 1½ hours, we actually took over 2. On the way, we crossed a rope bridge, right above the tunnel from last year.

{{ article_img(path="koppenkarstein/bridge.jpg", alt="The rope bridge above the Austriascharte") }}

From then on, the climb got a easier and easier, until we finally reached the glacier and our ride back down to the
parking lot.

{{ article_img(path="koppenkarstein/lookback.jpg", alt="Looking back at Koppenkarstein from the Dachstein glacier") }}

---

## Map
<iframe style="border:none" src="https://en.frame.mapy.cz/s/jucedugebe" width="100%" height="500" frameborder="0"></iframe>
