+++
title = "Dachstein 2023"
draft = false
date = 2023-05-01

[taxonomies]
categories = ["outdoors", "plans"]
+++

Dachstein-Überschreitung, am 22.-24.6.2023.

<!-- more -->

### Packliste

Hier mal eine Ausrüstungsliste. Gletscherausrüstung kann man sich bei diversen Alpenvereinssektionen ausleihen.

#### Gletcher- und Kletteraustrüstung

<style>
ul {
  list-style-type: none;
}
</style>

* [ ] Steigeisen
* [ ] Eispickel
* [ ] Klettergurt
* [ ] Kletterhelm
* [ ] ~5 Karabiner
* [ ] 2 Reepschnüre 5-6mm
* [ ] 1-2 Bandschlingen 120cm
* [ ] Klettersteigset
* [ ] 50m Seil (muss nur einer von uns mitnehmen)

#### Ausrüstung/Wanderzeug
* [ ] Wanderschuhe ;-)
* [ ] Sonnenbrille
* [ ] Regenjacke
* [ ] Warme Überjacke
* [ ] Kopftuch/Kappe/Hut
* [ ] Handschuhe
* [ ] Wandersocken
* [ ] Gamaschen oder Regenhose
* [ ] Sonnencreme
* [ ] Pflaster || Erste-Hilfe-Set
* [ ] Biwaksack
* [ ] Blasenpflaster?
* [ ] Handy
* [ ] Karte
* [ ] 2l Getränk
* [ ] Badehose? ;-)

#### Übernachten
* [ ] Schlafsack (Hütten- oder Sommerschlafsack)
* [ ] Wechselgewand für die Hütte
* [ ] Waschzeug/Hygieneartikel
* [ ] Handtuch
* [ ] Alpenvereins-Mitgliedskarte
* [ ] Geld
* [ ] Handyladegerät
* [ ] Medikamente?
* [ ] Spiele?

<script lang="javascript">
  let inputs = document.getElementsByTagName("input");
  for (let i of inputs) {
    let t = i.parentNode.textContent.trim();
    i.disabled = false;
    console.log(localStorage.getItem(t));
    if (localStorage.getItem(t) === "checked")  {
      i.checked = true;
    }
    i.onchange = function (el) {
      if (el.target.checked) {
        localStorage.setItem(t, "checked");
      } else {
        localStorage.setItem(t, "unchecked");
      }
    }
  }
</script>

### Donnerstag 22.6.

Start von Hallstatt zur Simonyhütte. Viele Höhenmeter, würde mit 5-6h reiner Gehzeit rechnen.

<iframe style="border:2px solid black" src="https://de.frame.mapy.cz/s/nasezagasa" width="100%" height="600" frameborder="0"></iframe>

### Freitag 23.6.

Dachsteinüberschreitung.

<iframe style="border:2px solid black" src="https://de.frame.mapy.cz/s/jahosasuzo" width="100%" height="600" frameborder="0"></iframe>

* Zustieg über Hallstätter Gletscher
* Gipfel über [Randkluftsteig oder Schulteranstieg](https://www.bergsteigen.com/fileadmin/userdaten/tour/topo/7729/randkluft-schulter-anstieg-klettersteig-dachstein-normalweg-topo.png)
* Abstieg zum Gosaugletscher über [Westgratsteig](https://www.bergsteigen.com/fileadmin/userdaten/import/topos/dachstein_westgrat_topo.jpg)
* Absteig zur Adamekhütte über Gosaugletscher

### Samstag 24.6.

Abstieg von der Adamekhütte.

<iframe style="border:2px solid black" src="https://de.frame.mapy.cz/s/pozamutala" width="100%" height="600" frameborder="0"></iframe>

* Geschätzt 4h Gehzeit
* Ankunft um die Mittagszeit am Vorderen Gosausee

# An- und Abreise

Nachdem wir nicht zum Ausgangspunkt zurückkehren, werden wir wohl teile der Strecke öffentlich zurücklegen.

Idee: Treffpunkt Bushaltestelle ["Gosaumühle"](https://de.mapy.cz/s/gazefoduza), von dort gibt es Busse nach Hallstatt,
bzw. aus Gosau. Außerdem einen Parkplatz für Anreise mit dem Auto.
