+++
title = "Gesäuse"
draft = false
date = 2022-05-26

[taxonomies]
categories = ["outdoors", "plans"]
+++

Wanderwochenende im Gesäuse, am 8./9.10.2022.

<!-- more -->

### Abfahrt

Treffpunkt **Samstag 6:55**, Bahnhof Meidling, Bahnsteig 5: [Öbb](https://fahrplan.oebb.at/webapp/?language=de_DE&ulVersion=1.1&context=TP&method=storageRecon&storageId=30b98d4f-4188-435e-9593-9bdb9dc1c0dd)

Bitte Ticket von Meidling nach Gstatterboden kaufen (oder Klimaticket mitnehmen 😜)

### Packliste

Ich vergesse sicher ein paar Sachen, aber als Orientierung:

#### Ausrüstung/Wanderzeug
* [ ] Wanderschuhe ;-)
* [ ] Helm
* [ ] Klettergurt
* [ ] Klettersteigset
* [ ] Regenjacke
* [ ] Blasenpflaster?
* [ ] Handy (Vorwarnung: kein Handyempfang auf der Hütte, erst ganz oben am Gipfel)
* [ ] Karte
* [ ] 2l Trinken
* [ ] Essen für Samstagmittag
* [ ] Fotoapparat?

#### Übernachten
* [ ] Schlafsack (Hütten- oder Sommerschlafsack)
* [ ] Wechselgewand für die Hütte
* [ ] Waschzeug/Hygieneartikel, soweit sinnvoll (es gibt keine Duschen!)
* [ ] Alpenvereins-Mitgliedskarte
* [ ] Geld (Kartenzahlung in der Hütte sollte möglich sein)
* [ ] Handyladegerät (es sollte eine einzelne Steckdose im Zimmer geben)
* [ ] Medikamente?
* [ ] Spiele?

### Rückfahrt

Zwei Möglichkeiten, je nachdem wie schnell wir am Sonntag sind:

* [17:12 von Gstatterboden, Ankunft 19:58 Meidling](https://fahrplan.oebb.at/webapp/?language=de_DE&ulVersion=1.1&context=TP&method=storageRecon&storageId=cc8e02e1-cf24-477f-82c8-33b842cbff96)
* [15:11 von Weng im Gesäuse, Ankunft 17:58 Meidling](https://fahrplan.oebb.at/webapp/?language=de_DE&ulVersion=1.1&context=TP&method=storageRecon&storageId=bee867f2-abfc-4e01-bd2d-f9febee362ec)

Vom Kölblwirt jeweils mit dem Gesäusetaxi (müssen wir rechtzeitig anmelden ~2h vorher)

### Samstag 8.10.

Start von Gstatterboden (oder Parkplatz Kummerbrücke). Aufstieg über den
[Wasserfallweg](https://www.bergsteigen.com/touren/klettersteig/wasserfallweg/) zur
[Hesshütte](https://www.diehesshuette.at/).

<iframe style="border:2px solid black" src="https://de.frame.mapy.cz/s/jomofomeha" width="100%" height="600" frameborder="0"></iframe>

* Viele Höhenmeter
* Wasserfallweg (Klettersteig bis B). Ausrüstung (Helm + Gurt + Set) empfohlen, Schwindelfreiheit absolut notwendig.
  (Ich habs letztes Jahr auch ohne geschafft, aber der Blick nach unten ist schon heftig).
* Bei Nässe ziemlich ungut. Alternative bei Schlechtwetter: Aufstieg über den Hartelsgraben und Sulzkar.
* Variante: Aufstieg zur Planspitze (500hm extra).

### Sonntag 9.10.

Bisher nur ein Vorschlag, können wir spontan entscheiden. Alternative Gipfel: Zinödl (einfaches Wandern),
Planspitze (letztes Stück leicht ausgesetzt).

Von der Hesshütte über den Guglgrat aufs Hochtor, Abstieg übers Schneeloch.

<iframe style="border:2px solid black" src="https://de.frame.mapy.cz/s/jaseceteco" width="100%" height="600" frameborder="0"></iframe>

* [Josefinensteig/Guglgrat](https://www.bergsteigen.com/touren/klettersteig/josefinensteig/) Kletterei 1/Klettersteig ~A-B. Oft kein Stahlseil, **Helm absolut notwendig**.
* [Schneeloch](https://www.bergfex.at/sommer/steiermark/touren/wanderung/29212,hochtor-ueber-das-schneeloch--josefinensteig--hess-huette/) Kletterei 1? Noch nie gegangen, hört sich ähnlich an wie Guglgrat, könnte unangenehm im Abstieg sein (ist der Josefinensteig aber auch).

### Anreise/Abreise

* Zug ins Gesäuse:
  * Am Wochenende um 7:02 von Meidling. Ankunft Gstatterboden 9:47 [Link](https://fahrplan.oebb.at/webapp/?language=de_DE&ulVersion=1.1&context=TP&method=storageRecon&storageId=30b98d4f-4188-435e-9593-9bdb9dc1c0dd)
  * Rückfahrt Sonntag 17:12, Ankunft 19:58 in Meidling [Link](https://fahrplan.oebb.at/webapp/?language=de_DE&ulVersion=1.1&context=TP&method=storageRecon&storageId=213c9ad9-c4e0-419d-9873-a7d0badbc8fb)
    * [Gseisspur Taxi](http://www.johnsbach.at/tourismus/sommer/gseispur.html) bringt uns vom Kölblwirt zum Bahnhof.
