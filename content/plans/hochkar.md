+++
title = "Vom Hochkar zum Dürrenstein"
draft = false
date = 2024-05-01

[taxonomies]
categories = ["outdoors", "plans"]
+++
In drei Tagen vom Hochkar zum Dürrenstein und nach Lunz am See, 30.5. bis 1.6.

<!-- more -->

### Tag 1, 30.5., Fronleichnam

Von Lassing zum Hochkar.

<iframe style="border:none" src="https://de.frame.mapy.cz/s/redohehoge" width="100%" height="600" frameborder="0"></iframe>

Anreise mit dem Bus nach Lassing.

<script type="text/javascript" src="https://anachb.vor.at/webapp/staticfiles/hafas-widget-core.1.0.0.js?L=vs_anachb&"></script>
<div data-hfs-widget="true" data-hfs-widget-tp="true" data-hfs-widget-tp-postform="newtab" data-hfs-widget-tp-dep="Wien Meidling" data-hfs-widget-tp-arr="Lassing (NÖ) Moosbauer" data-hfs-widget-tp-date="20240530" data-hfs-widget-tp-time="080000" data-hfs-widget-tp-enquiry="true"></div>

Quartier! Hochkar-Schutzhaus ist nicht geöffnet, alternative [JUFA-Hotel](https://www.jufahotels.com/hotel/hochkar/)?

### Tag 2, 31.5,

Vom Hochkar über den Dürrenstein zur Ybbstaler Hütte.

<iframe style="border:none" src="https://de.frame.mapy.cz/s/neduceraga" width="100%" height="600" frameborder="0"></iframe>

21,3 km! 1300hm! Sehr lang, sehr sonnig und ohne Hütte auf dem Weg!

Platz im Matrazenlager ist reserviert.

### Tag 3, 1.6.

Von der Ybbstaler Hütte nach Lunz am See.

<iframe style="border:none" src="https://de.frame.mapy.cz/s/desovubamo" width="100%" height="600" frameborder="0"></iframe>

Abreise mit dem Bus ab Lunz am See, entweder über Scheibbs oder Waidhofen/Ybbs, ca alle 2 Stunden, zB

<div data-hfs-widget="true" data-hfs-widget-tp="true" data-hfs-widget-tp-postform="newtab" data-hfs-widget-tp-date="20240601" data-hfs-widget-tp-time="150000" data-hfs-widget-tp-enquiry="true" data-hfs-widget-tp-dep="Lunz am See Schulen" data-hfs-widget-tp-arr="WIEN"></div>

### Änderungen

* Anderer Startpunkt als Hochkar? Doch recht lange Wanderung, Übernachtung im Hochkar relativ teuer
  * z.B. direkt von Lunz Aufsteigen, dafür dann über den Dürrenstein und Obersee absteigen?
