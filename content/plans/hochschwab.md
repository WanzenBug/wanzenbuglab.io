+++
title = "Hochschwab-überschreitung"
draft = false
date = 2022-05-26

[taxonomies]
categories = ["outdoors", "plans"]
+++
In drei Tagen von West nach Ost über den Hochschwab.

<!-- more -->

### Tag 1

Durch die Frauenmauerhöhle zur Sonnschienhütte

<iframe style="border:none" src="https://de.frame.mapy.cz/s/rejepanovo" width="100%" height="600" frameborder="0"></iframe>

* Höhlenführungen mit Führer im Sommer um 11:00 und 13:00

### Tag 2

Von der Sonnschienhütte über den Hochschwab zur Voitsthalerhütte.

<iframe style="border:none" src="https://de.frame.mapy.cz/s/galuhugefa" width="100%" height="600" frameborder="0"></iframe>

* Variante: nur bis zum Schiestlhaus

### Tag 3

Von der Voitsthalerhütte nach Seewiesen.

<iframe style="border:none" src="https://de.frame.mapy.cz/s/gofucupaho" width="100%" height="600" frameborder="0"></iframe>

* Sehr unspektakulärer letzter Tag

### Varianten

* Umgekehrt (von Seewiesen nach Eisenerz?): Kürzere erste Etappe, weniger Anreise-Stress?
  - Anreise nach Seewiesen: 9:05 ab meidling, Ankunft ~12:00 in Seewiesen.
  - Abreise ab Gsollkehre: 14:36, 16:36, 18:36, Ankunft ~3h später in Meidling
